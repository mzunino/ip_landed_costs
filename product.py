#  -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#    Copyright (C) 2010-2012 Camptocamp Austria (<http://www.camptocamp.at>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from tools.translate import _
import decimal_precision as dp
import pooler
import time
import math

#import ipdb


#----------------------------------------------------------
# Product INHERIT
#----------------------------------------------------------
class product_template(osv.osv):

    _inherit = "product.template"

    _columns = {
        'landed_cost_type': fields.selection([('value', 'Value'), ('per_unit', 'Quantity'), ('none', 'None')], "Distribution Type", help="If the product is defined as a landing cost, this controls how this cost will operates."),
        'landable'       :fields.boolean("Landing costs applies", help="Check this if the product applies for landing cost. Also implies it will be recalculated on every new product reception"),
    }

    _defaults = {
        # devuelve el valor de context['landed_cost_type'] o None
        'landed_cost_type': lambda self, cr, uid, context: context['landed_cost_type'] if 'landed_cost_type' in context else None,
        'landable': False,
    }

product_template()


#class product_historical(osv.osv):
class product_product(osv.osv):

    """ product_historical """

    _inherit = 'product.product'

    _columns = {

        'cost_historical_ids': fields.one2many('product.historic.cost','product_id','Historical Purchases Costs'),

    }

product_product()


class product_historic_cost(osv.osv):

    _order= "date desc"
    _name = "product.historic.cost"
    _description = "Product Historical Cost"
    _rec_name = "date"
    # get product name no porque se desordena la lista del hisrtóico de precio del producto tiene que ser ordenado por fecha

    _columns = {
        #'name': fields.char('Production Lot', size=64 ),
        'product_id': fields.many2one('product.product', string='Product related to this Cost', required=True),
        'product_uom': fields.many2one('product.uom', string="Supplier UoM", help="Choose here the Unit of Measure in which the prices and quantities are expressed below."),
        'date': fields.datetime(string='Date', required=True),
        'price': fields.float(string='Order Cost', digits_compute= dp.get_precision('Price'), help="Last purchase price"),
        'last_landed_cost': fields.float(string='Landed Cost', digits_compute= dp.get_precision('Price'), help="Last landed cost"),
        'stock_lot_id': fields.many2one('stock.production.lot', string="Lot Related"),
        'order_ref': fields.char('Related doc.chain', size=140, help='Origen', translate=True),
        #'lots_ids': fields.many2one('stock.production.lot', string="Lot"),
    }

    _defaults = {
        'date': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
    }

product_historic_cost()


class account_invoice_line(osv.osv):
    _inherit = "account.invoice.line"
    # it must be loaded at create pickings!
    _column = {
                'ul_cost': fields.one2many('ul.cost', 'invoice_line_id', "Cost updating", states={'approved':[('readonly',True)],'done':[('readonly',True)]}),
                'is_fake': fields.boolean('Guess', help="Check this if the final values may vary from today's ones."),

        }
account_invoice_line()



def _links_get(self, cr, uid, context={}):
    """
    Returns a list of tuples of 'model names' and 'Model title' to use as
    typs in reference fields.
    """
    landed_costs = self.pool.get('landed.cost.position')
    ids = landed_costs.search(cr, uid, [])

    res = landed_costs.read(cr, uid, ids, ['update_cost', 'name'], context)
    return [(r['update_cost'], r['name']) for r in res]

class ul_cost(osv.osv):
    '''
        Sets the relations beetween account_invoice_line and cost_position
        lots_ids and historical_cost.

        Updating historical cost manage.

        When PO become Stock Picking we save the invoice_line id related to a landing cost
        by loading this id on the table ul_cost

        En el pasaje de orden a remito, solo se podría marcar la línea de factura,
        poniendo el id de la linea de factura en la tabla mágica vinculado al costo.
        (esto ultimo es debil porque si el usuario borra la línea del remito este dato
        o se pierde o queda obsoleto.

        En tanto el costo asociado continuara con la bandera is_fake activada
        en el lcp vinculado al remito.

        Al aprobar el remito (recibir la merca) incorporamos el numero de lote
        y el del remito aprobado. Se podría además, marcar el lote y el histórico de costo correspondiente
    '''

    _order= "date desc"
    _name = "ul.cost"
    _description = "Varying landing costs after receiving goods"

    # ul.cost
    def _success(self, cr, uid, ids, field_name, arg, context=None):
        result = {}
        for cost in self.browse(cr, uid, ids, context):
            success = True
            updates = {}
            result[cost.id] = success
        return result

    # ul.cost
    def _default_cost_id(self, cr, uid, context=None):

        if context and context.get('reference_model', False):
            return '%s,%d' % ( context['reference_model'], context['reference_id'])
        else:
            return False



    _columns = {

        'name': fields.char('Number', size=64, required=True, select=True),
        'date': fields.datetime('Date', required=True, readonly=True,
                select=True, states={
                    'draft': [('readonly', False)],
                }),
        #'cost_id': fields.reference('Reference', selection=_links_get,
        #        size=128,  select=True),# states={ 'draft': [('readonly', False)], }),   readonly=True,

        #'success': fields.function(_success, method=True, type='boolean',
        #        string='Success', select=True, store=True,
        #        help='This field will be active if all updated costos have succeeded.'),

        #'state': fields.selection([
        #        ('draft', 'Draft'),
        #        ('waiting', 'Pending Supplier Invoicel'),
        #        ('success', 'Update Success'),
        #        ('failed', 'Update Failed'),
        #    ], 'State', readonly=True, select=True),
        #'company_id': fields.many2one('res.company', 'Company'),

        'invoice_line_id': fields.many2one('account.invoice.line', "Invoice Line Ref"),
        'product_historic_cost_id': fields.many2one('product.historic.cost', "Cost Related"),
        'prod_lot_id': fields.many2one('stock.production.lot', string="Lot Related"),
        'lc_position_id': fields.many2one('landed.cost.position', "Origin Landed Costs Position"),

        }

    _defaults = {
        'name': lambda obj, cr, uid, context: \
                obj.pool.get('ir.sequence').next_by_code(cr, uid, 'ul.cost'),
        'date': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
        #'state': 'draft',
        #'success': False,
        #'cost_id': _default_cost_id,
        #'company_id': lambda s, cr, uid, c: s.pool.get('res.company')\
        #        ._company_default_get(cr, uid, 'ul.cost', context=c),
    }

   #ir_sequence.next_by_code() or ir_sequence.next_by_id()
ul_cost()






'''
class product_category(osv.osv):

    _inherit = 'product.category'

    _columns = {

        'landed_cost': fields.boolean("Calculate Landed Costs", help="Check this if you want to use landed cost calculation for average price for this catgory"),

    }

product_category()

'''