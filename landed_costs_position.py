# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#    Copyright (C) 2010-2012 Camptocamp (<http://www.camptocamp.at>)
#    Copyright (C) 2013-2014 InfoPrimo (<http://www.infoprimo.com.uy>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import decimal_precision as dp
from tools.translate import _
import logging
import time

#import ipdb

class landed_cost_position(osv.osv):

    _name = "landed.cost.position"
    _description = "Expenses lines related to purchase goods"

    def _get_currency(self, cr, uid, context):
        ''' Returns: main company currency '''
        comp = self.pool.get('res.users').browse(cr,uid,uid).company_id
        if not comp:
            comp_id = self.pool.get('res.company').search(cr, uid, [])[0]
            comp = self.pool.get('res.company').browse(cr, uid, comp_id)
        return comp.currency_id.id

    def _line_cost_data(self, cr, uid, ids, field_name, args, context):
        ''' Returns: dict containing "ids" elements of landed.cost.position lines.
                     like id = { price, quantity, currency rate } each.
            [¿¿¿ 'parent': parent_obj is not a gotcha but a bug.
               in onchange_product_id it will be always None! ??]
        '''
        if not ids:
            return {}
        if context is None:
            context = {}
        parent_object = {}
        result = {}

        for cost in self.browse(cr, uid, ids):
            #TODO: Asegurar que ho existan costos sueltos.
                #  verificar los on_delete en los objetos que tengan "landed_cost_position" como "hijos"
                # En realidad no importa, se verifica al momento de ser usado, ya que se invoca por id
                # Se usa en landed.cost.position.landing_cost_lines_st y en purchase.order._create_pickings
            parent_obj =  cost.purchase_order_line_id or cost.purchase_order_id or cost.move_line_id or cost.picking_id or False

            parent_object.update( cost.purchase_order_line_id and { 'purchase_order_line': cost.purchase_order_line_id } \
                            or cost.purchase_order_id and { 'purchase_order': cost.purchase_order_id } \
                            or cost.move_line_id and { 'move_line': cost.move_line_id } \
                            or cost.picking_id and { 'picking': cost.picking_id } or False )
                        # or cost.invoice_line_id
            parent_name = parent_obj._name or False

            rate = cost.exchange_rate
            if rate is None:
                rate = 1
            if cost.price_type == 'value':
                qty = 1
                result[cost.id] = { 'amo': cost.amount, 'qty': qty, 'rate': rate, 'parent': parent_obj }
            else:
                if parent_name in ('purchase.order', 'stock.picking'):
                    qty = parent_obj.quantity_total
                elif parent_name in ('purchase.order.line', 'stock.move'):
                    qty = parent_obj.product_qty
                else:
                    raise osv.except_osv(_('Error'), \
                    _('Something was wrong... \nPlease, take note \n\n Cost: %s   Parent: %s   Ids: %s' % (cost, parent_obj, ids) ))
                result[cost.id] = { 'amo': float(cost.amount), 'qty': qty, 'rate': float(rate), 'parent': parent_obj  }

        return result

    def _landing_cost_lines_st(self, cr, uid, ids, name, args, context):
        ''' Returns: landing cost position line sub-total amount
            # expresed in 2nd currency '''

        if not ids :
            return {}
        if context is None:
            context = {}

        result = {}
        res = {}

        res = self._line_cost_data(cr, uid, ids, name, args, context)

        for cost in self.browse(cr, uid, ids):
            if res[cost.id] and cost:
                result[cost.id] = ( res[cost.id]['amo'] * res[cost.id]['qty'] )    /     float(res[cost.id]['rate'])
        return result

    _columns = {
        'purchase_order_id': fields.many2one('purchase.order', 'Purchase Order'),
        'purchase_order_line_id': fields.many2one('purchase.order.line', 'Purchase Order Line'),
        'picking_id': fields.many2one('stock.picking', 'Picking'),
        'move_line_id': fields.many2one('stock.move', 'Picking Line'),
        'invoice_line_id': fields.many2one('account.invoice.line', 'Invoice Line related (weak)'),
        'is_fake': fields.boolean('Guess', help="Check this if the final values may vary from today's ones."),

        'name': fields.char('Name', size=34, required=True, help='Origen', translate=True),
        'product_id': fields.many2one('product.product','Landed Cost Name', required=True, domain=[('landed_cost_type','!=', False)]),
        'partner_id': fields.many2one('res.partner', 'Partner', help="The supplier of this cost component.", required=True),
        'currency_id': fields.many2one('res.currency', 'Curr.', help="Optional other currency."),
        'exchange_rate': fields.related('currency_id', 'rate', type='float', string='Exchange Rate', digits=(12,6)),
        'amount': fields.float('Amount', required=True, digits_compute=dp.get_precision('Purchase Price'), \
                                help="Landed cost for stock valuation. It will be added to the supplier price."),
        'amount_currency': fields.float('Amt.Currency', help="The amount expressed in another optional currency."),
        'price_type': fields.selection( [('per_unit','Per Quantity'), ('value','Absolute Value')], 'Type', required=True,  \
                                help="Defines whether the value will be considered by its absolute amount \'Per Value\' or it should be multiplied by the moved units \'Per Quantity\'"),
        'line_cost_data': fields.function(_line_cost_data, string='PO Line Data', method=True),
        'landing_cost_lines_st': fields.function(_landing_cost_lines_st, digits_compute=dp.get_precision('Account'), string='STotal Cost', method=True),

        #'update_cost': fields.one2many('ul.cost', 'lc_position_id', "Cost updating", states={'approved':[('readonly',True)],'done':[('readonly',True)]}),
        #SELECT ail.id, ail.origin, ail.name, ai.state,ail.price_unit,ail.price_subtotal,ail.quantity,ail.partner_id,ail.product_id from account_invoice_line ail, account_invoice ai where ail.invoice_id = 9 and ai.id =9 ;
    }

    _defaults = {
        'name': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
        'currency_id': _get_currency,
        'is_fake': False
    }
    '''
    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        print"---------Calling copy function----"
        print default
        default.update({
            'purchase_order_id': [],
            'purchase_order_line_id': [],
            'picking_id': [],
            'move_line_id': [],
        })
        print default
        return super(landed_cost_position, self).copy(cr, uid, id, default, context)
    '''
    def create(self, cr, uid, vals, context=None):

        if not vals.get('name'):
            vals['name'] = time.strftime('%Y-%m-%d %H:%M:%S')
        #ipdb.set_trace()
        landable = False
        vals_keys = vals.keys()

        if 'purchase_order_id' in vals_keys:
            order = self.pool.get('purchase.order').browse(cr, uid,[vals['purchase_order_id']])[0]

            landable = order.landable

        elif 'purchase_order_line_id' in vals_keys:
            line = self.pool.get('purchase.order.line').browse(cr, uid,[vals['purchase_order_line_id']])[0]
            landable = line.product_id.landable

        elif 'picking_id' in vals_keys:
            picking = self.pool.get('stock.picking').browse(cr, uid,[vals['picking_id']])[0]
            #landable = stock.picking[0].landable(self, cr, uid, ids)[vals['picking_id']]:
            landable = True

        elif 'move_line_id' in vals_keys:
            move_line = self.pool.get('stock.move').browse(cr, uid,[vals['move_line_id']])[0]
            #landable = stock.move[0].landable(self, cr, uid, ids)[vals['move_line_id']]:
            landable = True
        if not landable:
                warn = { 'error': _('Error'), 'message': _('The order could not be saved. Possible cause: there\'s a landing cost in a not lendable product line or the Order have no landable products at all. Please, check your entries.') }
                raise osv.except_osv(_('Error'), warn['message'])

        res =  super(landed_cost_position, self).create(cr, uid, vals, context)

        return True

#--------------------------------------------------------------
#  using *2many fields in on_change methods
#
#  https://doc.openerp.com/trunk/server/06_misc_on_change_tips/
#--------------------------------------------------------------

    def onchange_product_id(self, cr, uid, ids, product_id, amount, context):
        '''
            Load related values form product_id, like price_type, providers, etc.
        '''

        vals = {}
        if context is None:
            context = {}
        if product_id:
            prod_obj = self.pool.get('product.product')
            prod = prod_obj.browse(cr, uid, [product_id])[0]
            if prod.landed_cost_type:
                vals = {'price_type': prod.landed_cost_type}
            if prod.product_tmpl_id.seller_ids:
                vals.update({'partner_id': prod.product_tmpl_id.seller_ids[0].name.id})
            else:
                # TODO: blank field to prevents last selected partner to be displayed
                # vals.update({})
                pass
            if prod.standard_price:
                vals.update({'amount': prod.standard_price})
        return {'value': vals }

    def onchange_currency_id(self, cr, uid, ids, currency_id, amount, price_type, context=None):

        vals = {}
        if context is None:
            context = {}
        if currency_id:
            obj_currency = self.pool.get('res.currency')
            rate = obj_currency.browse(cr, uid, currency_id).rate
            if rate:
                vals = {'amount_currency': amount / rate}
            else:
                raise osv.except_osv(_('Error'), _('New currency is not configured properly !'))

        return {'value': vals}

    def onchange_amount(self, cr, uid, ids, amount, currency_id, price_type, context=None):
        '''on change amount, amount_currency must be recalculate
        if currency_id (optional) <> main_company_currency'''
        vals = {}
        if context is None:
            context = {}
        company_currency = self._get_currency(cr, uid, context)
        if currency_id and currency_id != company_currency:
            amount = float(amount)
            obj_currency = self.pool.get('res.currency')
            rate = float(obj_currency.browse(cr, uid, currency_id).rate)
            if rate:
                vals = {'amount_currency': amount / rate}
            else:
                raise osv.except_osv(_('Error'), _('New currency is not configured properly !'))

        return {'value': vals}

    def onchange_amount_currency(self, cr, uid, ids, amount_currency, amount, currency_id, price_type, context=None):
        result = vals = {}
        if context is None:
            context = {}
        if currency_id:
            obj_currency = self.pool.get('res.currency')
            rate = obj_currency.browse(cr, uid, currency_id).rate
            if rate:
                vals = {'amount': amount_currency * rate}
            else:
                raise osv.except_osv(_('Error'), _('The currency, the rate or both are not configured properly !'))

        return {'value': vals}

landed_cost_position()

