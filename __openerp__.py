# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#    Copyright (C) 2010-2012 Camptocamp (<http://www.camptocamp.com>)
#    Copyright (C) 2014 Infoprimo.SL (<http://www.infoprimo.com.uy>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Landed Costs',
    'version': '0.9',
    'category': 'Purchase Management',
    'description': """
    Include:
    Landing costs in the average price computation & Historical for landed and purchased cost.
    ==============================================

    The landed costs can be defined for

            * purchase orders
            * purchase order lines
            * picking
            * receiving

    The landing costs are in turn, simples products or services in the ERP. A distribution method
    for these products or services must be indicate at the product or service record. So you may
    set the costs to be considerate by its absolute value for all the purchase order or to be
    multiply by units to purchased quantity

    "Distribution Type" is defined in the product record either by "Value" or "Quantity"

    In order to automatically calculate costs on a given product, the concerned product should
    be flagged. Use "Calculate landed costs" check-box to calculate landing cost on the product price.

    A new landed cost for will be calculated when a receive or picking order is done. Related
    incoming lot number is stored jointly to the new calculated price for historic or
    any further purposes.

    A draft invoice for the service cost is automatically created when an Purchase Order or
    Incoming Shipment containing a product affected by landing costs is confirmed.

    This module also handles both the historical "purchase price" and "landed cost" of a product.

    Important note: only Weight UdM Category based products can be targeted for landing costs.


    """,
    'author': 'Infoprimo SL, Camptocamp, Vauxoo',
    'depends': ["purchase","decimal_precision","stock"],
    'update_xml': [
                    'ulc_sequence.xml',
                    'purchase_view.xml',
                    'stock_view.xml',
                    'product_view.xml',
                    'data/costos_importacion.xml',
                    'data/product_decimal.xml',
                    'security/ir.model.access.csv',
                    'landed_costs_position_view.xml',
                    'wizard/stock_partial_picking_view.xml',
                    'wizard/stock_partial_move_view.xml',
                   ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
