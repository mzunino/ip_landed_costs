# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#    Copyright (C) 2010-2012 Camptocamp (<http://www.camptocamp.at>)
#    Copyright (C) 2013-2014 InfoPrimo (<http://www.infoprimo.com.uy>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import netsvc
import pooler
import decimal_precision as dp
from osv.orm import browse_record, browse_null
from tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from tools.translate import _
import logging

import time
from datetime import datetime
from dateutil.relativedelta import relativedelta

import ipdb

def resolve_o2m(o2m_command):
    """
     @params: list((int|False, int|False, dict|False)) a tuple command update a relational filed

        relational fields semantic in write/update/delete methods
        +--------------------+-------------+---------------------------------------------------+
        | command            |result value*|  Details                                          |
        |--------------------+-------------+---------------------------------------------------|
        |(0, 0,  { fields }) |          1  |  create  *                                        |
        |(1, ID, { fields }) |          0  |  update  *                                        |
        |(2, ID)             |         -1  |  remove (delete)                                  |
        |(3, ID)             |         -1  |  unlink one (target id or target of relation)     |
        |(4, ID)             |          1  |  link_to *                                        |
        |(5)                 |          0  |  unlink all (only valid for one2many)             |
        |(6, 0, [IDs])       |          0  |  replace  (don't matter since it doesn't change   |
        |                    |             |            cost relevance for the line order)     |
        +--------------------+-------------+---------------------------------------------------+
        o2m command codes
        ^^^^^^^^^^^^^^^^^
        cost_in  : "0 : CREATE",  "1 : UPDATE",     "4 : LINK_TO"
        cost_out : "2 : REMOVE",  "3 : UNLINK_ONE", "5 : UNLINK_ALL"

     @returns:  dict { id or 0 :  *result value }

    """
    res = {}
    if True: # there were a "for" here, and nobody knows the future...
        if isinstance(o2m_command, tuple):

            if   o2m_command[0] == 0:
                res['create'] =     { 0: 1 }

            elif o2m_command[0] == 1:
                res['update'] =     { int(o2m_command[1]) :  0 }

            elif o2m_command[0] == 2:
                res['remove'] =     { int(o2m_command[1]) : -1 }

            elif o2m_command[0] == 3:
                res['unlink_one'] = { int(o2m_command[1]) : -1 }

            elif o2m_command[0] == 4:
                res['link_to'] =    { int(o2m_command[1]) :  1 }

            elif o2m_command[0] >= 5:
                res['unlink_replace'] =  { 0: 0 }

            else:
                raise osv.except_osv(_('Error !'), _('It is a relational field that is none of (0,1,2,3,4,5,6) commands! \\nPlease see \"resolve_o2m_commands_to_record_dicts\" at openerp/osv/orm.py on line 4940 or something alike.'))
    return res

#----------------------------------------------------------
# Purchase Line INHERIT
#----------------------------------------------------------
class purchase_order_line(osv.osv):
    _inherit = "purchase.order.line"

    _logger = logging.getLogger(__name__)

    def _landing_cost_val(self, cr, uid, ids, name, args, context):
        """PO Lines Total Landing Costs x Value"""
        # Returns: $ PO Lines Value costs
        if not ids:
            return {}
        result = {}
        # landed costs for the order.line
        for line in self.browse(cr, uid, ids):
            lc = 0.0
            if line.landed_cost_position_ids:
                for costs in line.landed_cost_position_ids:
                    if costs.price_type == 'value':
                        lc += costs.amount
            result[line.id] = lc
        return result

    def _landing_cost_qty(self, cr, uid, ids, name, args, context):
        """ PO Lines Total Landing Costs x Quantity)"""
        # Returns: $ PO Lines quantity costs
        if not ids:
            return {}
        result = {}
        # landed costs for the order.line
        for line in self.browse(cr, uid, ids):
            lc = 0.0
            if line.landed_cost_position_ids:
                for costs in line.landed_cost_position_ids:
                    if costs.price_type == 'per_unit':
                        lc += costs.amount * line.product_qty
            result[line.id] = lc
        return result

    def _landing_cost_pl(self, cr, uid, ids, name, args, context):
        """ PO Lines Total Landing Costs (xvalue + xquantity)"""
        # Returns: $ value + quantity costs
        if not ids:
            return {}
        result = {}
        # landed costs for the order.line
        for line in self.browse(cr, uid, ids):
            lc = 0.0
            if line.landed_cost_position_ids:
                for costs in line.landed_cost_position_ids:
                    if costs.price_type == 'value':
                        lc += costs.amount
                    else:
                        lc += costs.amount * line.product_qty
            result[line.id] = lc
        return result

    # _landing_from_pick
    def _landing_from_order(self, cr, uid, ids, name, args, context):
        """
            Returns: weighing of total head costs on each line.
            (landing cost from order / line_amount)
        """
        if not ids:
            return {}
        result = {}

        for line in self.browse(cr, uid, ids):
            lc = 0.0
            if line.order_id.landed_cost_position_ids:
                # order's lines summed -untaxed
                if line.order_id.ip_amount_total > 0.0:
                    lc += line.order_id.landing_cost_base_val / \
                    line.order_id.ip_amount_total * line.price_unit * line.product_qty
                # order's quantity of purchased products
                if line.order_id.quantity_total > 0.0:
                    lc += line.order_id.landing_cost_base_qty / \
                    line.order_id.quantity_total * line.product_qty

            result[line.id] = lc
        return result

    # _landed_cost_sm
    def _landed_cost_pl(self, cr, uid, ids, name, args, context):

        """
          Returns: $ ( tot amount purchase.line ) + costs from line
        """
        if not ids:
            return {}
        result = {}
        # landed costs for the line
        for line in self.browse(cr, uid, ids):
            landed_cost_pl = 0.0
            for cost in line.landed_cost_position_ids:
                lc = 0.0
                if cost.price_type == 'value':
                    lc += cost.amount
                else:
                    lc += cost.amount * line.product_qty
                if lc > 0:
                    landed_cost_pl += line.product_qty * line.price_unit + lc
            result[line.id] = landed_cost_pl
        return result

    def _sub_total(self, cr, uid, ids, name, args, context):
        # Line sub total $$ product purchase
        if not ids:
            return {}
        result = {}
        for line in self.browse(cr, uid, ids):
            sub_total = 0.0
            sub_total += line.product_qty * line.price_unit or 0.0
            result[line.id] = sub_total
        return result

    def _avg_landed_cost_pl(self, cr, uid, ids, name, args, context):
        """
        Returns: averaged po line price or avg line order landed cost
        """
        if not ids:
            return {}
        if context is None:
            context = {}
        result = {}
        for pl in self.browse(cr, uid, ids):
            if pl.product_qty > 0:
                result[pl.id] = (pl.landed_cost_pl + pl.landing_from_order) / pl.product_qty
        return result

    _columns = {
         'price_unit': fields.float('Unit Price', required=True, digits_compute= dp.get_precision('Purchase Price 3d'), help='Unit Price. High on precision: 3 digits.'),
         'landing_enable': fields.boolean('Enable Costs', help='It must be checked in order to support landing costs. Please note, for uncheck it you must delete every landing cost for this line and save your order', state={'approved':[('readonly',True)],'done':[('readonly',True)]}),
         'landed_cost_position_ids': fields.one2many('landed.cost.position', 'purchase_order_line_id', 'Landed Costs Positions (line)', state={'approved':[('readonly',True)],'done':[('readonly',True)]}),

         'landing_cost_val': fields.function(_landing_cost_val, digits_compute=dp.get_precision('Account'), string='Landing Costs - Val'),
         'landing_cost_qty': fields.function(_landing_cost_qty, digits_compute=dp.get_precision('Account'), string='Landing Costs - Qty'),
         'landing_cost_pl' : fields.function(_landing_cost_pl, digits_compute=dp.get_precision('Account'), string='S.T. Landing Costs in Line'),

         'landing_from_order' : fields.function(_landing_from_order, digits_compute=dp.get_precision('Account'), string='S.T. Landing Costs from Order'),

         'landed_cost_pl' : fields.function(_landed_cost_pl, digits_compute=dp.get_precision('Account'), string='Purchase Landed Costs'),
         'sub_total' : fields.function(_sub_total, digits_compute=dp.get_precision('Account'), string='Line Sub Total'),
         'avg_landed_cost_pl': fields.function(_avg_landed_cost_pl, digits_compute=dp.get_precision('Account'), string='Line Landed Price', help="Order Line Landed Cost"),
    }

    _default = { 'landing_enable': True }


    def onchange_landing_enable(self, cr, uid, ids, landing_enable, product_id, context):
        """
            Sirve para no abrir líneas de costo en una linea no costeable.
            Sirve para no cerrar lineas de costo en una línea con costos asignados.
        """

        if not ids:
            return {}
        if context is None:
            context = {}
        warn = {}
        vals = {}

        line = self.browse(cr, uid, ids)[0]
        product_line = line.product_id

        if product_line.id != product_id:
            product_line = self.pool.get('product.product').browse(cr, uid, [product_id])[0]

        if landing_enable:

            if product_line.landable:
                vals = {'landing_enable': landing_enable}
            else:
                msg0 = _('Please note "')
                msg1 = "%s" % (product_line.name,)
                msg2 = _('" is not a landable product. Otherwise you may want to save the order before enabling landing costs.')
                landing_enable = False
                vals = {'landing_enable': landing_enable}
                warn = {'title': _('Error'), 'message': msg0 + msg1 + msg2 }
        else:

            if line.landed_cost_position_ids:
                landing_enable = True
                vals = {'landing_enable': landing_enable}
                warn = { 'title': _('Error'), 'message': _('Disabling landing cost tab in a line with landing costs!\nPlease, drop your costs lines before disabling or just save your order if apply.') }
            else:
                vals = {'landing_enable': landing_enable}
        return  { 'value': vals, 'warning': warn }

    def onchange_product_id(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, notes=False, context=None):

        print "ingreso en onchange_product_id"
        #if not ids:
	#    print "sin ids"
        #    return {}

        '''
        Original
        '''

        if context is None:
            context = {}
        
        res = {'value': {'price_unit': price_unit or 0.0, 'name': name or '', 'notes': notes or '', 'product_uom' : uom_id or False}}
        if not product_id:
	    print "sin product_id"
            return res

        product_product = self.pool.get('product.product')
        product_uom = self.pool.get('product.uom')
        res_partner = self.pool.get('res.partner')
        product_supplierinfo = self.pool.get('product.supplierinfo')
        product_pricelist = self.pool.get('product.pricelist')
        account_fiscal_position = self.pool.get('account.fiscal.position')
        account_tax = self.pool.get('account.tax')

        # - check for the presence of partner_id and pricelist_id
        if not pricelist_id:
            raise osv.except_osv(_('No Pricelist !'), _('You have to select a pricelist or a supplier in the purchase form !\nPlease set one before choosing a product.'))
        if not partner_id:
            raise osv.except_osv(_('No Partner!'), _('You have to select a partner in the purchase form !\nPlease set one partner before choosing a product.'))

        # - determine name and notes based on product in partner lang.
        lang = res_partner.browse(cr, uid, partner_id).lang
        context_partner = {'lang': lang, 'partner_id': partner_id}
        product = product_product.browse(cr, uid, product_id, context=context_partner)
        res['value'].update({'name': product.partner_ref, 'notes': notes or product.description_purchase})
        print product
        # - set a domain on product_uom
        res['domain'] = {'product_uom': [('category_id','=',product.uom_id.category_id.id)]}

        # - check that uom and product uom belong to the same category
        product_uom_po_id = product.uom_po_id.id
        if not uom_id or context.get('force_product_uom'):
            uom_id = product_uom_po_id
        
        if product.uom_id.category_id.id != product_uom.browse(cr, uid, uom_id, context=context).category_id.id:
            res['warning'] = {'title': _('Warning'), 'message': _('Selected UOM does not belong to the same category as the product UOM')}
            uom_id = product_uom_po_id

        res['value'].update({'product_uom': uom_id})

        # - determine product_qty and date_planned based on seller info
        if not date_order:
            date_order = fields.date.context_today(cr,uid,context=context)

        qty = qty or 1.0
        supplierinfo = False
        supplierinfo_ids = product_supplierinfo.search(cr, uid, [('name','=',partner_id),('product_id','=',product.id)])
        if supplierinfo_ids:
            supplierinfo = product_supplierinfo.browse(cr, uid, supplierinfo_ids[0], context=context)
            if supplierinfo.product_uom.id != uom_id:
                res['warning'] = {'title': _('Warning'), 'message': _('The selected supplier only sells this product by %s') % supplierinfo.product_uom.name }
            min_qty = product_uom._compute_qty(cr, uid, supplierinfo.product_uom.id, supplierinfo.min_qty, to_uom_id=uom_id)
            if qty < min_qty: # If the supplier quantity is greater than entered from user, set minimal.
                res['warning'] = {'title': _('Warning'), 'message': _('The selected supplier has a minimal quantity set to %s %s, you should not purchase less.') % (supplierinfo.min_qty, supplierinfo.product_uom.name)}
                qty = min_qty

        dt = self._get_date_planned(cr, uid, supplierinfo, date_order, context=context).strftime(DEFAULT_SERVER_DATETIME_FORMAT)

        res['value'].update({'date_planned': date_planned or dt, 'product_qty': qty})

        # - determine price_unit and taxes_id
        price = product_pricelist.price_get(cr, uid, [pricelist_id],
                    product.id, qty or 1.0, partner_id, {'uom': uom_id, 'date': date_order})[pricelist_id]
        
        taxes = account_tax.browse(cr, uid, map(lambda x: x.id, product.supplier_taxes_id))
        fpos = fiscal_position_id and account_fiscal_position.browse(cr, uid, fiscal_position_id, context=context) or False
        taxes_ids = account_fiscal_position.map_tax(cr, uid, fpos, taxes)
        res['value'].update({'price_unit': price, 'taxes_id': taxes_ids})

        '''
        Hasta aquí Original

        res =  super(purchase_order_line, self).onchange_product_id(cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order, fiscal_position_id, date_planned, name, price_unit, notes, context)
        '''
        
        #product_obj = self.pool.get('product.product').browse(cr, uid, [product_id])[0]

        #if not product_obj.landable:
        #    po_line = self.browse(cr, uid, ids[0])
        #    if po_line.landed_cost_position_ids:
        #        res['warning'] =  { 'title': _('Error'), 'message': _('This line has costs. Please, drop your costs lines before changing the product, of just save your order if apply.') }
        if not product.landable:
            po_line = self.browse(cr, uid, ids[0])
            if po_line.landed_cost_position_ids:
                res['warning'] =  { 'title': _('Error'), 'message': _('This line has costs. Please, drop your costs lines before changing the product, of just save your order if apply.') }
        
        return res

    def create(self, cr, uid, vals, context=None):
        """
            @return: Returns an id of the new record
        """
        if context is None:
            context = {}
        res_id = False
        product = self.pool.get('product.product').browse(cr, uid, vals['product_id'])
        if context.get('order') and vals['order']:
            print vals['order']
        if vals.get('landed_cost_position_ids', False):
            if product.landable:
                res_id = super(purchase_order_line, self).create(cr, uid, vals, context=context)
            else:
                raise osv.except_osv(_('Error !'), _('We can\'t save costs in some of your lines.\nPlease check your entries.'))
        else:
            res_id = super(purchase_order_line, self).create(cr, uid, vals, context=context)
        return res_id

    def write(self, cr, uid, ids, vals, context=None):
        """
            @return True or False | raise exception
            Note: if a "Fill the red fields" error is reached, please
            check if the "res" variable ever get the super(...) function
        """
        if not ids:
            return {}
        if context is None:
            context = {}
        res = False

        if (vals.get('state',False) or vals.get('invoiced',False)) and 'landed_cost_position_ids' not in vals:
        # TODO: check if 'state' or 'invoiced' without 'landed_cost_position_ids'
        #       in the vals dictionary implies an only a status change.
        #       In such case verify about costs is unnecessary.


            res = super(purchase_order_line, self).write(cr, uid, ids, vals, context=context)

        elif len(vals) > 1:

            if isinstance(ids, (int, long)): ids = [ids]

            ## resolve if there are cost in the line  (db)
            line_data = self.browse(cr,uid,ids)
            in_line_cost = []
            for line in line_data:
                if line.landed_cost_position_ids:
                    for cost in line.landed_cost_position_ids:
                        in_line_cost.append(1)

            ## resolv if there will be cost after write (vals)
            cost_after_write = False
            if vals.get('landed_cost_position_ids') and vals['landed_cost_position_ids']:
                in_vals_cost = []
                for cost in vals['landed_cost_position_ids']:
                    cv = resolve_o2m(cost)
                    in_vals_cost.append(cv.values()[0].values()[0])
                if sum(in_line_cost) + sum(in_vals_cost):
                    cost_after_write = True
            else:
                if sum(in_line_cost):
                    cost_after_write = True

            ## resolve if there will be a landable line alter write   (vals)
            if vals.get('product_id', False) and vals['product_id'] != line_data[0].product_id.id:
                landable_to_write = self.pool.get('product.product').browse(cr, uid, vals['product_id']).landable
            else:
                landable_to_write = line.product_id.landable

            if cost_after_write and not landable_to_write:
                raise osv.except_osv(_('Error !'), _('There are some costs hanging over a not landable line.\nPlease, check your entries.'))
            else:
                res = super(purchase_order_line, self).write(cr, uid, ids, vals, context=context)
        return res


purchase_order_line()

#--------------------------------------------------------
#   Purchase Order INHERIT
#--------------------------------------------------------
class purchase_order(osv.osv):
    _inherit = "purchase.order"
    _logger = logging.getLogger(__name__)

    # --------------------------------------------------------------------
    # Costs Methods
    def _landing_cost_lines_qty(self, cr, uid, ids, name, args, context):
        """ Calculates the partial per Qty. Costs affected to order.line """
        if not ids:
            return {}
        result = {}
        for order in self.browse(cr, uid, ids):
            landing_cost_lines_qty = 0.0
            for pol in order.order_line:
                for lcp in pol.landed_cost_position_ids:
                    if lcp.price_type == 'per_unit' and pol.product_qty > 0.0:
                        landing_cost_lines_qty += lcp.amount * pol.product_qty
            result[order.id] = landing_cost_lines_qty
        return result

    def _landing_cost_lines_val(self, cr, uid, ids, name, args, context):
        """ Return: the partial per Value Costs affected to order.line """
        if not ids:
            return {}
        result = {}
        for order in self.browse(cr, uid, ids):
            landing_cost_lines_value = 0.0
            for pol in order.order_line:
                for lcp in pol.landed_cost_position_ids:
                    if lcp.price_type == 'value':
                        landing_cost_lines_value += lcp.amount
            result[order.id] = landing_cost_lines_value
        return result

    def _landing_cost_lines(self, cr, uid, ids, name, args, context):
        """ Returns: the sum of costs in the line order (value costs + qty costs)"""
        if not ids:
            return {}
        result = {}
        for order in self.browse(cr, uid, ids):
            landed_cost_lines = 0.0
            if order.order_line:
                for pol in order.order_line:
                    if pol.product_qty > 0.0:
                        landed_cost_lines += pol.landing_cost_pl
            result[order.id] = landed_cost_lines
        return result


    def _landing_cost_base_qty(self, cr, uid, ids, name, args, context):
        """Calculates the partial per Qty. Costs afected to entire order"""
        if not ids:
            return {}
        result = {}
        for order in self.browse(cr, uid, ids):
            landing_costs_base_quantity = 0.0
            if order.landed_cost_position_ids:
                for costs in order.landed_cost_position_ids:
                    if costs.price_type == 'per_unit':
                        landing_costs_base_quantity += costs.amount * order.quantity_total
            result[order.id] = landing_costs_base_quantity
        return result


    def _landing_cost_base_val(self, cr, uid, ids, name, args, context):
        """Calculates the partial per Value Costs afected to entire order """
        if not ids:
            return {}
        result = {}
        for order in self.browse(cr, uid, ids):
            lcb = 0.0
            if order.landed_cost_position_ids:
                for costs in order.landed_cost_position_ids:
                    if costs.price_type == 'value':
                        lcb += costs.amount
            result[order.id] = lcb
        return result


    def _landing_cost_base(self, cr, uid, ids, name, args, context):
        """ Returns: the sum of costs in body order (value + qty costs)
        :rtype : dict
        """
        if not ids:
            return {}
        if context is None:
            context = {}
        result = kys = its = {}

        qty = self._landing_cost_base_qty(cr, uid, ids, name, args, context)
        val = self._landing_cost_base_val(cr, uid, ids, name, args, context)

        kys = list(set(qty.keys() + val.keys()))
        its = qty.items() + val.items()

        results = dict.fromkeys(kys, 0.0)
        for i in its:
            results[i[0]] += i[1]
        return results

    def _landing_cost(self, cr, uid, ids, name, args, context):
        """
        Sum of (cost from lines + costs from order) for po. orders
        """
        if not ids:
            return {}
        if context is None:
            context = {}
        result = kys = its = {}
        ords = self._landing_cost_base(cr, uid, ids, name, args, context)
        lins = self._landing_cost_lines(cr, uid, ids, name, args, context)

        kys = list(set(ords.keys() + lins.keys()))
        its = ords.items() + lins.items()

        results = dict.fromkeys(kys, 0.0)
        for i in its:
            results[i[0]] += i[1]
        return results

    def _landed_cost(self, cr, uid, ids, name, args, context):
        """
        Purchase Order Total Landed Costs (xvalue + xquantity) from orders.
        The method walks all POs, ask for landing costs -regardless its
        value or quantity nature- and sums them. Finally if the sum > 0 add
        it's added to the PO total untaxed.
        Returns: Total costs untaxed from order (qty + value based ones)
        """
        if not ids:
            return {}
        result = {}

        for order in self.browse(cr, uid, ids):
            landed_cost = 0.0
            if order.landing_cost > 0:
                landed_cost += order.landing_cost  + order.amount_untaxed
            else:
                landed_cost += order.amount_untaxed
            result[order.id] = landed_cost

        #### ojo que puede ser sin grasa de lana! y sin costos !
        return result

    def _quantity_total(self, cr, uid, ids, name, args, context):
        """ Returns: total quantity -UoM in order """

        # TODO  It would be useful to discriminate those qtys that don't
        #       carry costs at all.
        # This may occur when there're no costs at the purchase order base
        # in those orders with multiple lines order. In turn some of its
        # order lines has associated costs but others lines has not.

        if not ids:
            return {}
        result = {}
        for order in self.browse(cr, uid, ids):
            quantity_total = 0.0
            if order.order_line:
                for pol in order.order_line:
                    if pol.product_qty > 0.0:
                         quantity_total += pol.product_qty
            result[order.id] = quantity_total
        return result



    def _subt_untaxed(self, cr, uid, ids, name, args, context):
        """
        Subtotal Purchase Orded -untaxed- openerp don't like to shows twice a single field
        Returns the total untaxed amount for the order
        """
        if not ids:
            return {}
        result = {}
        for order in self.browse(cr, uid, ids, context=context):
            result[order.id] = order.amount_untaxed
        return result


    #_subt_untaxed en stock
    def _amount_total(self, cr, uid, ids, name, args, context):
        # sub_total lines sumed - untaxed
        if not ids:
            return {}
        result = {}
        for line in self.browse(cr, uid, ids):
            atot = 0.0
            if line.order_line:
                for ml in line.order_line:
                    if ml.product_qty > 0.0 and ml.price_unit:
                         atot += ml.sub_total
            result[line.id] = atot
        return result

    def _avg_landed_price_order(self, cr, uid, ids, name, args, context):
        """
        Returns: averaged landed order price (on order with severals lines)
        """
        if not ids:
            return {}
        if context is None: context = {}
        result = kys = its = {}
        cos = self._landed_cost(cr, uid, ids, name, args, context)
        kgs = self._quantity_total(cr, uid, ids, name, args, context)

        if cos and kgs:
            kys = list(set(cos.keys() + kgs.keys()))
            for i in kys:
                if kgs[i] != 0:
                    result[i] = cos[i]/kgs[i]
                else:
                    result[i] = 0
        return result

    def _avg_price_order(self, cr, uid, ids, name, args, context):
        """
            Returns: averaged order price (on order with severals lines)
        """
        if not ids:
            return {}
        if context is None:
            context = {}
        result = {}
        for order in self.browse(cr, uid, ids, context=context):
            if order.quantity_total:
                result[order.id] = order.amount_total / order.quantity_total
            else:
                result[order.id] = 0
        return result

    def _landable(self, cr, uid, ids, field_name, arg, context=None):
        """
            Return form: { po0 : bool, po1 : bool, ... }
            where "bool" is True if purchase order has at least one line with
            a landable product otherwise "bool" is False
        """
        if not ids:
            return {}
        if context is None:
            context = {}
        result = {}

        for order in self.browse(cr, uid, ids):
            if order:
                result[order.id] = False
                for pol in order.order_line:
                    if pol and pol.product_id.landable:
                        result[order.id] = True
                        break
        return result


    _columns = {
         'landing_enable' : fields.boolean('Enable Landing Costs', help='It must be checked in order to support landing costs. Please note, for uncheck this box you must delete every landing cost either from lines or order and save your order.',
                                        state={'approved':[('readonly',True)],'done':[('readonly',True)]}),
         'landable': fields.function(_landable, type='boolean', string='Order has landable product(s) line(s)', method=True),

         'landed_cost_position_ids': fields.one2many('landed.cost.position', 'purchase_order_id', 'Landed Costs Positions (order)', state={'approved':[('readonly',True)],'done':[('readonly',True)]}),

         'landing_cost_lines_qty': fields.function(_landing_cost_lines_qty, digits_compute=dp.get_precision('Account'), string='Landing Cost Lines - Qty'),
         'landing_cost_lines_val': fields.function(_landing_cost_lines_val, digits_compute=dp.get_precision('Account'), string='Landing Cost Lines - Value'),
         'landing_cost_lines'    : fields.function(_landing_cost_lines, digits_compute=dp.get_precision('Account'), string='S.Total Landing Cost in Lines'),

         'landing_cost_base_qty' : fields.function(_landing_cost_base_qty, digits_compute=dp.get_precision('Account'), string='Landing Costs From Order - Qty'),
         'landing_cost_base_val' : fields.function(_landing_cost_base_val, digits_compute=dp.get_precision('Account'), string='Landing Costs From Order - Value'),
         'landing_cost_base' : fields.function(_landing_cost_base, digits_compute=dp.get_precision('Account'), string='S.Total Landing Costs in Order'),

         'landing_cost' : fields.function(_landing_cost, digits_compute=dp.get_precision('Account'), string='Total Landing Costs'),
         'landed_cost' : fields.function(_landed_cost, digits_compute=dp.get_precision('Account'), string='Purchased Landed Costs'),

         'quantity_total' : fields.function(_quantity_total, digits_compute=dp.get_precision('Product UoM'), string='Total Quantity'),
         'subt_untaxed': fields.function(_subt_untaxed, digits_compute=dp.get_precision('Account'), string='Total Purchased Untaxed', method=True),

         'ip_amount_total': fields.function(_amount_total, digits_compute=dp.get_precision('Account'), string='Total Amount', method=True),
         'avg_landed_price_order': fields.function(_avg_landed_price_order, digits_compute=dp.get_precision('Account'), string='Purchase Landed Price Order'),
         'avg_price_order': fields.function(_avg_price_order, digits_compute=dp.get_precision('Account'), string='Purchase Price Order'),

         'incoterm': fields.many2one('stock.incoterms', 'Incoterm', help="Incoterm which stands for 'International Commercial terms' implies its a series of sales terms which are used in the commercial transaction."),
         'perline_invoice': fields.boolean('Separate shipments', help='Checked the box if you want to generate per line draft invoices.',
                                        states={'approved':[('readonly',True)],'done':[('readonly',True)]}),

    }

    _default = { 'landing_enable': True }

    def _cost_enable(self, cr, uid, ids, context):
        """
            Return form: { po0: bool, poX: bool, ... }
            where "bool" is True if landing costs tab is enabled for the order,
            otherwise "bool" is False.
        """
        if not ids:
            return {}
        if context is None:
            context = {}
        result = {}
        for order in self.browse(cr, uid, ids):
            result[order.id] = order.landing_enable
        return result

    def _cost_exist(self, cr, uid, ids, context=None):
        """
            Return form { po0 : bool, po1 : bool, ... }
            where "bool" is True if landed_cost_position "order cost"
            [or line.landed_cost_position "line cost"] is
            founded, otherwise "bool" will be False
        """
        if not ids:
            return {}
        if context is None:
            context = {}
        result = {}
        # ipdb.set_trace()
        for order in self.browse(cr, uid, ids):
            result[order.id] = False
            if order.landed_cost_position_ids:
                result[order.id] = True
                break
            '''
            elif order.order_line:
                for line in order.order_line:
                    if line.landed_cost_position_ids:
                         result[order.id] = True
            '''

        return result

    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        print"--------- Calling copy function --------"
        print default
        default.update({
            'landing_enable' : True,
            'landed_cost_position_ids': [],
            'state':'draft',
            'shipped':False,
            'invoiced':False,
            'invoice_ids': [],
            'picking_ids': [],
            'name': self.pool.get('ir.sequence').get(cr, uid, 'purchase.order'),
        })
        _logger(default)
        print default
        return super(purchase_order, self).copy(cr, uid, id, default, context)

    def create(self, cr, uid, vals, context=None):
        """
            @return: Returns an id of the new record
        """

        if context is None:
            context = {}
        res = False
        landable = False
        if vals.get('landed_cost_position_ids') and vals['landed_cost_position_ids']:
            # cost is comming at order level then look for a landable product in lines
            product_pool = self.pool.get('product.product')
            landable = False
            if vals.get('order_line', False) and vals['order_line']:
                for product_line in vals['order_line']:
                    if isinstance(product_line, (tuple)) and product_line[0] in [0,4]:
                        if 'product_id' in product_line[2]:
                            landable = product_pool.browse(cr, uid, product_line[2]['product_id']).landable
                            break
                if landable:
                    res = super(purchase_order, self).create(cr, uid, vals, context=context)
                else:
                    raise osv.except_osv( _('Error'),
                        _('Attempt to assigning landing costs in an order with no landable lines.\nPlease, check your entries.') )
            else:
                raise osv.except_osv( _('Error'),
                            _('Attempt to assigning landing costs in an order with no lines.\nPlease, add some lines to your order and try again.') )
        else:
            res = super(purchase_order, self).create(cr, uid, vals, context=context)

        return res


    def write(self, cr, uid, ids, vals, context=None):
        """
            @return True or False | raise exception
            Note: if a "Fill the red fields" error is reached, please
            check if the "res" variable ever get the super(...) function
        """

        if context is None:
            context = {}
        if not ids:
            return {}
        res = False

        if len(ids) > 1:
            raise osv.except_osv( _('ERROR'), _('Hay más de un id para actualizar! Por favor, pida asistencia.') )

        if vals.get('state',False) and len(vals) == 1:
            res = super(purchase_order, self).write(cr, uid, ids, vals, context=context)

        elif len(vals) > 1:

            if isinstance(ids, (int, long)): ids = [ids]

            ## resolve if there are o.cost on the line (bd)
            ## resolve if there ar landable lines in order (db)
            order_data = self.browse(cr,uid,ids)
            in_order_cost = []
            landable_in_order = []
            for order in self.browse(cr,uid,ids,context):
                if order.landable:
                    landable_in_order.append(1)
                if order.landing_cost_base:
                    in_order_cost.append(1)

            ## resolve if there will be o.costs after write  (vals)
            order_cost_after_write = False
            if 'landed_cost_position_ids' in vals and vals['landed_cost_position_ids']:
                in_vals_cost = []
                for cost in vals['landed_cost_position_ids']:
                    cv = resolve_o2m(cost)
                    in_vals_cost.append(cv.values()[0].values()[0])
                if sum(in_order_cost) + sum(in_vals_cost):
                    order_cost_after_write = True
            else:
                if sum(in_order_cost):
                    order_cost_after_write = True

            ## resolve if there will be a landable line alter write (vals)
            landable_order_after_write = False
            if 'order_line' in vals and vals['order_line']:
                landable_in_vals = []
                for line in vals['order_line']:
                    lv = resolve_o2m(line)
                    landable_in_vals.append(lv.values()[0].values()[0])
                if sum(landable_in_order) + sum(landable_in_vals):
                    landable_order_after_write = True
            else:
                if sum(landable_in_order):
                    landable_order_after_write = True

            if order_cost_after_write and not landable_order_after_write:
                raise osv.except_osv(_('Error !'), _('oThere are some costs in the order but any landable lines.\nPlease, check your entries.'))
            else:
                res =  super(purchase_order, self).write(cr, uid, ids, vals, context=context)

        return res

    def _prepare_inv_line(self, cr, uid, account_id, order_line, context=None):
        """
        Collects require data from purchase order line that is used to create invoice line
        for that purchase order line

        :@param account_id: Expense account of the product of PO line if any.
        :@param browse_record order_line: Purchase order line browse record
        :@return: Value for fields of invoice lines.
        :@rtype: dict
        """

        # until now, this (override) method is here for documentation only.
        results =  super(purchase_order, self)._prepare_inv_line(cr, uid, account_id, order_line, context=None)

        """
        results =  {
            'name': order_line.name,
            'account_id': account_id,
            'price_unit': order_line.price_unit or 0.0,
            'quantity': order_line.product_qty,
            'product_id': order_line.product_id.id or False,
            'uos_id': order_line.product_uom.id or False,
            'invoice_line_tax_id': [(6, 0, [x.id for x in order_line.taxes_id])],
            'account_analytic_id': order_line.account_analytic_id.id or False,
        }
        """

        return results

    def action_invoice_create(self, cr, uid, ids, context=None):
        """
        Generates invoice for given ids of purchase orders and links that invoice ID to purchase order.
        :param ids: list of ids of purchase orders.
        :return: ID of created invoice.
        :rtype: int
        """

        res = False

        if context is None:
            context = {}
        if not ids:
            return res

        journal_obj = self.pool.get('account.journal')
        inv_obj = self.pool.get('account.invoice')
        inv_line_obj = self.pool.get('account.invoice.line')

        for order in self.browse(cr, uid, ids, context=context):
            if not order.perline_invoice:
                res = super(purchase_order, self).action_invoice_create(cr, uid, ids, context)
            else:
                pay_acc_id = order.partner_id.property_account_payable.id
                journal_ids = journal_obj.search(cr, uid, [('type', '=','purchase'),('company_id', '=', order.company_id.id)], limit=1)
                if not journal_ids:
                    raise osv.except_osv(_('Error !'),
                        _('There is no purchase journal defined for this company: "%s" (id:%d)') % (order.company_id.name, order.company_id.id))

                # generate invoice line correspond to PO line and link that to created invoice (inv_id) and PO line
                for po_line in order.order_line:
                    acc_id = self._choose_account_from_po_line(cr, uid, po_line, context=context)

                    inv_line_data = self._prepare_inv_line(cr, uid, acc_id, po_line, context=context)
                    # create the account_invoice_line
                    inv_line_id = inv_line_obj.create(cr, uid, inv_line_data, context=context)

                    # set True invoiced field in origial purchase order line
                    po_line.write({'invoiced':True, 'invoice_lines': [(4, inv_line_id)]}, context=context)

                    # get invoice head data and create invoice
                    inv_data = {
                        'name': order.partner_ref or order.name,
                        'reference': order.partner_ref or order.name,
                        'account_id': pay_acc_id,
                        'type': 'in_invoice',
                        'partner_id': order.partner_id.id,
                        # TODO: what if not US$ ???  answ: goods purchase are ever U$S!
                        'currency_id': order.pricelist_id.currency_id.id,

                        'address_invoice_id': order.partner_address_id.id,
                        'address_contact_id': order.partner_address_id.id,
                        'journal_id': len(journal_ids) and journal_ids[0] or False,
                        'invoice_line': [(6, 0, inv_line_id)],
                        'origin': order.name,
                        'fiscal_position': order.fiscal_position.id or order.partner_id.property_account_position.id,
                        'payment_term': order.partner_id.property_payment_term and order.partner_id.property_payment_term.id or False,
                        'company_id': order.company_id.id,
                    }
                    inv_id = inv_obj.create(cr, uid, inv_data, context=context)

                    # compute the invoice
                    inv_obj.button_compute(cr, uid, [inv_id], context=context, set_total=True)

                    # Link this new invoice to related purchase order
                    order.write({'invoice_ids': [(4, inv_id)]}, context=context)
                    res = inv_id
        return res

    def _prepare_order_line_move(self, cr, uid, order, order_line, picking_id, context=None):
        # when order is going to be confirmed then backup the original purchase price to 'price_unit_net'
        # and give landed price results to 'price_unit'
        #import ipdb
        #ipdb.set_trace()
        res = super(purchase_order,self)._prepare_order_line_move( cr, uid, order, order_line, picking_id, context)
        res['price_unit_net'] =  res['price_unit']
        res['price_unit'] = order.avg_price_order
        res['landing_enable'] = True # order_line.landing_enable
        res['line_invoiced'] = order_line.invoiced
        return res

    def _prepare_order_picking(self, cr, uid, order, context=None):
        #ipdb.set_trace()
        res = super(purchase_order, self)._prepare_order_picking( cr, uid, order, context)
        res['landing_enable'] = True
        return res

    def _get_product_account_expense_id(self, product):
        """
        Returns the product's account expense id if present
        or it's parent categories account expense id otherwise
        """
        if product.property_account_expense.id:
            return product.property_account_expense.id

        return product.categ_id.property_account_expense_categ.id

    def check_costs(self, porder=None, order_lines=None):
        """
        Checks:
                Order lines costs: ensure that every cost position hangs from a
                                    order line containing a landable product.
                Order costs: if there are costs at order level, then we need at
                                least one line containing a landable product.
        @return: True or a warning tuple
        """
        warn_chk =  ( _('Purchase Order General Error'), _('Unknown error occurs. Please call for support.') )
        landable = False
        # At least one order with lines , otherwise we've nothing to do.
        if order_lines and porder:
            # verify that each of not landable product line lacks costs.
            for line in order_lines:
                landable = line.product_id.landable
                if line.landed_cost_position_ids and not landable:
                        warn_chk = ( _('Error Cheking Order Lines Costs'), _('There\'s some line cost position in not landable lines!\nPlease, check your order lines.') )
            if not landable:
                # no landable lines were found
                # is there some costs at order head?
                if porder.landed_cost_position_ids:
                    # and there's costs at porder head
                    warn_chk = ( _('Error Checking Order Costs'), _('There\'s some order cost position but not landable lines!\nPlease, double check your order.') )
                else:
                    # not landable lines and not head porder costs
                    landable = True

        else:
            # not order nor lines!
            warn_chk = ( _('Purchase Order General Error'), _('No lines in the order to process, please check!') )

        return landable or warn_chk

    # class purchase_order(osv.osv): _create_pickings es un método de la po.
    def _create_pickings(self, cr, uid, order, order_lines, picking_id=False, context=None):
        """
            Runs whenever products are received from providers.

            - create draft invoices from each cost position of the order head
              also do so whith each cost positions from each order_line

            - loops costs positions from order and link them to pickings
              also do so whith cost positions from order_line

             products invoices remain as usual (not doing here!)

        """

        chk = self.check_costs(order, order_lines)

        if isinstance(chk, tuple):
            raise osv.except_osv( chk[0], chk[1] )

        if context is None:
            context = {}

        # ipdb.set_trace()
        print """ if there are costs, then `landing_enable` for pickings as default """


        # get the new picking id / create the picking
        res =  super(purchase_order, self)._create_pickings(cr, uid, order, order_lines, picking_id, context)
        print res, "\n\t>>>>>>>>>>\t\t ESTE es el supuesto ide del picking cuando convierte la orden en remito \n<<<<<<<<<\n"

        line_costs = []
        for cost in order_lines:
            line_costs.append(1) if cost.landed_cost_position_ids else line_costs.append(0)

        # si del cabezal de la orden cuelgan costos
        #                   OR
        # si de alguna de las líneas cuelgan costos
        # la marcancía costeada no se factura aquí!
        if order.landed_cost_position_ids or sum(line_costs):

            # get the landed cost object
            landed_cost_position_obj = self.pool.get('landed.cost.position')

            pick_id = int(res[0])
            invoice_obj = self.pool.get('account.invoice')
            invoice_line_obj = self.pool.get('account.invoice.line')
            journal_obj = self.pool.get('account.journal')
            journal_ids = journal_obj.search(cr, uid, [('type', '=','purchase'),('company_id', '=', order.company_id.id)], limit=1)
            cmsg = _('Landing Cost - ')

            # create invoices of costs from po header
            for order_cost in order.landed_cost_position_ids:

                # INVOICEING costs
                # getting/setting values for invoice head - from po header costs
                vals_inv = {
                            'partner_id': order_cost.partner_id.id,
                            'currency_id': order_cost.currency_id.id or order.company_id.currency_id.id,
                            'account_id': order_cost.partner_id.property_account_payable.id,
                            'type': 'in_invoice',
                            'origin': cmsg + order.name,
                            'fiscal_position':  order.partner_id.property_account_position and order.partner_id.property_account_position.id or False,
                            'company_id': order.company_id.id,
                            'journal_id': len(journal_ids) and journal_ids[0] or False,
                            'address_invoice_id' : order_cost.partner_id.address[0].id
                }

                # creating the incoice (head) for costs for the order costs
                inv_id = invoice_obj.create(cr, uid, vals_inv, context=None)

                # getting/setting values for invoice lines de COSTOS
                vals_inv_line = {

                            'product_id': order_cost.product_id.id,
                            'name': order_cost.product_id.name,
                            'account_id': self._get_product_account_expense_id(order_cost.product_id),
                            'partner_id': order_cost.partner_id.id,
                            'invoice_id': inv_id, # the order id for the line
                            'price_unit': order_cost.amount / order_cost.exchange_rate,
                            'quantity': order_cost.line_cost_data['qty'],
                            'invoice_line_tax_id': [(6, 0, [x.id for x in order_cost.product_id.supplier_taxes_id])],
                }

                # get the new invice line id / create the line for cost from order lines - CREA LINEA de FACTURA de COSTOS
                inv_line_id = invoice_line_obj.create(cr, uid, vals_inv_line, context=None)

                # re-use values for the "new" cost position
                vals_costs_head = {}
                vals_costs_head['name'] = order.name + ': ' + time.strftime('%Y-%m-%d %H:%M:%S')
                vals_costs_head['product_id'] = order_cost.product_id.id
                vals_costs_head['partner_id'] = order_cost.partner_id.id
                vals_costs_head['amount'] = order_cost.amount
                vals_costs_head['amount_currency'] = order_cost.amount_currency
                vals_costs_head['currency_id'] = order_cost.currency_id.id
                vals_costs_head['price_type'] = order_cost.price_type
                vals_costs_head['picking_id'] = pick_id
                vals_costs_head['is_fake'] = order_cost.is_fake
                if order_cost.is_fake:
                    vals_costs_head['invoice_line_id'] = inv_line_id
                    # es esto u otra cosa.


                # Re-create costs positions (from po order head) to the picking re- CREA COST POSITION para picking cabezal del remito
                landed_cost_position_obj.create(cr, uid, vals_costs_head, context=None)


            # get the picking object
            pick_obj = self.pool.get('stock.picking')

            # itera stock.pickings sobre el picking recién creado, o sea la copia de la orden
            for pick in pick_obj.browse(cr, uid, [pick_id], context=None):

                # itera líneas de stock.move (líneas del picking)
                for pml in pick.move_lines:

                    # itera los costs_position de las líneas de PO pertenecientes a líneas de stock.picking
                    # es acá donde genara las líneas de factura correspondientes a los costos de importación
                    # que cuelguen de las líneas de "órden" [ahora remito]
                    # también acá es donde recreará los costos de importación de dichas líneas.
                    for order_cost in pml.purchase_line_id.landed_cost_position_ids:

                        vals_inv = {
                                    'partner_id': order_cost.partner_id.id,
                                    'currency_id': order_cost.currency_id.id or order.company_id.currency_id.id,
                                    'account_id': order_cost.partner_id.property_account_payable.id,
                                    'type': 'in_invoice',
                                    'origin': cmsg + order.name,
                                    'fiscal_position':  order.partner_id.property_account_position and order.partner_id.property_account_position.id or False,
                                    'company_id': order.company_id.id,
                                    'journal_id': len(journal_ids) and journal_ids[0] or False,
                                    'address_invoice_id' : order_cost.partner_id.address[0].id
                        }

                        # invoice_id / create invoice cabezal de la factura de los costos de línea FACTURA COSTOS del PO.line ...
                        inv_id = invoice_obj.create(cr, uid, vals_inv, context=None)

                        # getting/setting values for invoice head - from po lines costs ARMA el CABEZAL de FACTURA de las líneas de costos del po
                        vals_inv_line = {
                                    'product_id': order_cost.product_id.id,
                                    'name': order_cost.product_id.name,
                                    'account_id': self._get_product_account_expense_id(order_cost.product_id),
                                    'partner_id': order_cost.partner_id.id,
                                    'invoice_id': inv_id, # the invoice_id to the line
                                    'price_unit': order_cost.amount / order_cost.exchange_rate,
                                    'quantity': order_cost.line_cost_data['qty'],
                                    'invoice_line_tax_id': [(6, 0, [x.id for x in order_cost.product_id.supplier_taxes_id])],
                        }

                        # create invoice_line líneas de factura de los costos de las líneas de la orden de compra FACTURA LINEAS de COSTO
                        inv_line_id = invoice_line_obj.create(cr, uid, vals_inv_line, context=None)
                        # si cuelgan costos de las líneas de la orden de compra

                        vals_costs_line = {}
                        vals_costs_line['name'] = order.name + ': ' + time.strftime('%Y-%m-%d %H:%M:%S')
                        vals_costs_line['product_id'] = order_cost.product_id.id
                        vals_costs_line['partner_id'] = order_cost.partner_id.id
                        vals_costs_line['amount'] = order_cost.amount
                        vals_costs_line['amount_currency'] = order_cost.amount_currency
                        vals_costs_line['currency_id'] = order_cost.currency_id.id
                        vals_costs_line['price_type'] = order_cost.price_type
                        vals_costs_line['move_line_id'] = pml.id
                        vals_costs_line['is_fake'] = order_cost.is_fake
                        if order_cost.is_fake:
                            vals_costs_line['invoice_line_id'] = inv_line_id
                        # u otra cosa
                        # Re-create costs positions (from line pickings) to the picking - re CREA COSTOS para MOVE (lineas del remito)
                        landed_cost_position_obj.create(cr, uid, vals_costs_line, context = None)

        return res

purchase_order()
