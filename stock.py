# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#    Copyright (C) 2010-2012 Camptocamp Austria (<http://www.camptocamp.at>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import decimal_precision as dp
import logging
from tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from tools.translate import _
import time
import string

#import ipdb

class stock_location(osv.osv):
    ''' Add “Admisión Temporaria” Location '''

    _name = "stock.location"
    _description = "Location"
    _inherit = "stock.location"

    _columns = {
            'usage': fields.selection([('supplier', 'Supplier Location'), ('view', 'View'), ('internal', 'Internal Location'), ('customer', 'Customer Location'), ('inventory', 'Inventory'), ('procurement', 'Procurement'), ('production', 'Production'), ('transit', 'Transit Location for Inter-Companies Transfers'), ('at_transit', 'Goods on Temporary Admission')], 'Location Type', required=True,
                 help="""* Supplier Location: Virtual location representing the source location for products coming from your suppliers
                       \n* View: Virtual location used to create a hierarchical structures for your warehouse, aggregating its child locations ; can't directly contain products
                       \n* Internal Location: Physical locations inside your own warehouses,
                       \n* Customer Location: Virtual location representing the destination location for products sent to your customers
                       \n* Inventory: Virtual location serving as counterpart for inventory operations used to correct stock levels (Physical inventories)
                       \n* Procurement: Virtual location serving as temporary counterpart for procurement operations when the source (supplier or production) is not known yet. This location should be empty when the procurement scheduler has finished running.
                       \n* Production: Virtual counterpart location for production operations: this location consumes the raw material and produces finished products
                       \n* Goods on Temporary Admission: Goods in transit for processing and to export back.
                      """, select = True),
         # temporarily removed, as it's unused: 'allocation_method': fields.selection([('fifo', 'FIFO'), ('lifo', 'LIFO'), ('nearest', 'Nearest')], 'Allocation Method', required=True),
        }

stock_location()

class stock_production_lot(osv.osv):

    _name = 'stock.production.lot'
    _description = 'Production lot'
    _inherit = 'stock.production.lot'

    def _get_lot_cost(self, cr, uid, ids, name, args, context):
        if not ids:
            return {}
        if context is None:
            context = {}
        result = {}
        obj_prod_hist = self.pool.get('product.historic.cost')
        for lot in self.browse(cr, uid, ids):
            domain = [('product_id', '=', lot.product_id.id), ('name', '=', lot.date)]
            result[lot.id] = obj_prod_hist.search(cr, uid, domain)
        return result

    _columns = {
        # esto vendría después que tengas solucionado el mantenimiento del histórico.!!
        # supongo que es un trigger en el workfolw de stock.picking/move
        # es aquí los métodos serían : do_change_standard_price, y do_partial
        # habrá que sobreescribir métodos para mechar allí la actualización del
        # landed cost ???
        #    ./stock.py
        #    ./wizard/stock_partial_move.py
        #    ./wizard/stock_partial_picking.py
        #    ./wizard/stock_change_standard_price.py
        #    ./product.py
        #landed_cost_lot_id
        #'historic_cost_ids': fields.one2many('product.historic.cost', 'product_id', string="Cost Lot"),
        #'landed_cost_lot_id': fields.many2one('product.historic.cost', 'Landed Cost Date'),
        #'lot_landed_cost': fields.float("Landed Cost", digits_compute=dp.get_precision('Account'), help="The final const of the product for this lot"),
        #'get_lot_cost' : fields.function(_get_lot_cost, digits_compute=dp.get_precision('Account'), string='Lot Cost'),
    }

    def copy(self, cr, uid, id, default=None, context=None):
        if context is None: context = {}
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.update(date=time.strftime('%Y-%m-%d %H:%M:%S'), revisions=[], move_ids=[])
        return super(stock_production_lot, self).copy(cr, uid, id, default=default, context=context)


stock_production_lot()


#----------------------------------------------------------
#  Stock Move
#---------------- LINEA de REMITO -------------------------
#----------------------------------------------------------

class stock_move(osv.osv):
    _inherit = "stock.move"

    def _landing_costs_sm(self, cr, uid, ids, name, args, context):
        ''' Picking Total Landing Costs (xvalue + xquantity)
        suma ambos tipos de costos en cada una de las líneas
        del remito : Total costos de las líneas del remito '''
        if not ids : return {}
        #print 'landing_costs_sm ids move: %s \n' % ids
        result = {}
        # landed costs for the line
        for move in self.browse(cr, uid, ids):
            # itera los costos que existen para la línea
            landing_costs_sm = 0.0
            for cost in move.landed_cost_position_ids:
                if cost.price_type == 'value':
                    landing_costs_sm += cost.amount
                else:
                    landing_costs_sm += cost.amount * move.product_qty
            result[move.id] = landing_costs_sm
        #print move.picking_id.name, result, 'costos x moves landing_costs_sm\n'
        return result

    # _landed_cost_pl de purchase.py
    def _landed_cost_sm(self, cr, uid, ids, name, args, context):
        ''' Return: total facturado move sin impuesto + costos de move (landed_cost_position_ids correspondientes a al move)
        no incluye prorrateo de costos de cabazal
        '''
        if not ids : return {}
        if context is None: context = {}
        result = {}
        for move in self.browse(cr, uid, ids):
            landed_cost_sm = 0.0
            for cost in move.landed_cost_position_ids:
                if cost.price_type == 'value':
                    landed_cost_sm += cost.amount
                else:
                    landed_cost_sm += cost.amount * move.product_qty
            result[move.id] = move.product_qty * move.price_unit_net + landed_cost_sm
        return result

    #_landing_cost_order de purchase.py
    def _landing_from_pick(self, cr, uid, ids, name, args, context):
        # Returns: costs poderated from picking land into line
        # Not include $ amount of purchased woods.
        if not ids : return {}
        result = {}

        for move in self.browse(cr, uid, ids):
            lc = 0.0
            # distribution of landed costs of PO
            if move.picking_id.landed_cost_position_ids:
                if move.picking_id.subt_untaxed > 0.0:
                    lc += move.picking_id.landing_cost_pick_val / \
                    move.picking_id.subt_untaxed * move.price_unit_net * move.product_qty

                if move.picking_id.quantity_total > 0.0:
                    lc += move.picking_id.landing_cost_pick_qty / \
                    move.picking_id.quantity_total * move.product_qty

            result[move.id] = lc
        return result

    def _sub_total_sm(self, cr, uid, ids, name, args, context):
        ''' Importe de la línea sin costos ni impuestos'''
        if not ids : return {}
        result = {}
        for move in self.browse(cr, uid, ids):
            if (move.product_qty > 0.0 ) and (move.price_unit_net > 0.0):
                result[move.id] = move.product_qty * move.price_unit_net
        return result


    def _avg_landed_cost_move(self, cr, uid, ids, name, args, context):
        '''
        Returns: averaged move price or avg line move landed cost
        '''
        #print "--> _avg_landed_cost_mov ids: %s \n" % ids

        if not ids:
            return {}
        if context is None:
            context = {}
        result = {}
        for sm in self.browse(cr, uid, ids):
            if sm.product_qty > 0:
                result[sm.id] = (sm.landed_cost_sm + sm.landing_from_pick) / sm.product_qty
        #print result, '_avg_landed_cost_move <--'
        return result

    _columns = {
        'landing_enable': fields.boolean('Enable Costs', help='It must be checked in order to support landing costs', state={'cancel':[('readonly',True)],'done':[('readonly',True)],'waiting':[('readonly',True)],'confirmed':[('readonly',True)]}),
        'landed_cost_position_ids': fields.one2many('landed.cost.position', 'move_line_id', 'Move Costs Positions', state={'approved':[('readonly',True)],'done':[('readonly',True)]}),
        'landing_costs_sm' : fields.function(_landing_costs_sm, digits_compute=dp.get_precision('Account'), string='Move Landing Costs'),
        'landed_cost_sm' : fields.function(_landed_cost_sm, digits_compute=dp.get_precision('Account'), string='Landed Costs Move'),
        'landing_from_pick' : fields.function(_landing_from_pick, digits_compute=dp.get_precision('Account'), string='Landing Costs from Picking'),
        'sub_total_sm' : fields.function(_sub_total_sm, digits_compute=dp.get_precision('Account'), string='Move Sub Total'),
        'price_unit_net' : fields.float('Purchase Price', digits_compute=dp.get_precision('Account'), ),
        #'avg_landed_cost_move': fields.function(_avg_landed_cost_move, method=True, type='float', digits_compute=dp.get_precision('Account'), string='Move Landed Average Price', store=True, help="Move Landed Cost"),
        'avg_landed_cost_move': fields.function(_avg_landed_cost_move, digits_compute=dp.get_precision('Account'), string='Move Landed Average Price', help="Move Landed Cost"),
    }

    def change_landing_enable(self, cr, uid, ids, landing_enable, context=None):
        '''
            Sirve para no abrir líneas de costo en una linea no costeable.
            Sirve para no cerrar lineas de costo en una línea con costos asignadoss.
        '''
        if not ids:
            return {}
        if context is None:
            context = {}
        warn = {}
        vals = {}
        #ipdb.set_trace()

        line = self.browse(cr, uid, ids)[0]
        if landing_enable:

            if line.product_id.landable:
                vals = {'landing_enable': landing_enable}
            else:
                msg0 = "%s" % (line.product_id.name,)
                msg1 = _('Please note \"')
                msg2 = _('\" is not a landable product, otherwise you may want to save the shippment before enabling landing costs.')
                msg = msg1 + msg0 + msg2
                landing_enable = False
                vals = {'landing_enable': landing_enable}
                warn = {'title': _('Error'), 'message': msg }
        else:
            cost_exist = line.landed_cost_position_ids

            if cost_exist:
                landing_enable = True
                vals = {'landing_enable': landing_enable}
                warn = { 'title': _('Error'), 'message': _('Attempt to clear landing cost section in a line with loaded landing costs! Please, drop your costs lines before disabling landing costs or save your documment if apply.') }
            else:
                vals = {'landing_enable': landing_enable}

        return  { 'value': vals, 'warning': warn }

stock_move()


#----------------------------------------------------------
# Stock Picking
#------------------ CABEZAL de REMITO ---------------------
#----------------------------------------------------------

class stock_picking(osv.osv):
    _inherit = "stock.picking"

    def _landing_cost_move_qty(self, cr, uid, ids, name, args, context):
        ''' Return: stock_move costs x qty '''
        if not ids : return {}
        result = {}
        for pick in self.browse(cr, uid, ids):
            lcmq = 0.0
            for m in pick.move_lines:
                for cost in m.landed_cost_position_ids:
                    if cost.price_type == 'per_unit':
                        lcmq += cost.amount * m.product_qty
            result[pick.id] = lcmq
        return result

    def _landing_cost_move_val(self, cr, uid, ids, name, args, context):
        ''' Return: stock_move costs x value '''
        if not ids : return {}
        result = {}
        for pick in self.browse(cr, uid, ids):
            lcmv = 0.0
            for m in pick.move_lines:
                for cost in m.landed_cost_position_ids:
                    if cost.price_type == 'value':
                        lcmv += cost.amount
            result[pick.id] = lcmv
        return result

    def _landing_cost_move(self, cr, uid, ids, name, args, context):
        ''' Return: stock_move costs (x value) + (x qty)'''
        if not ids : return {}
        result = {}
        for pick in self.browse(cr, uid, ids):
            landed_cost_move = 0.0
            if pick.move_lines:
                for m in pick.move_lines:
                    if m.product_qty > 0.0:
                        landed_cost_move += m.landing_costs_sm
            result[pick.id] = landed_cost_move
        return result

    def _landing_cost_pick_qty(self, cr, uid, ids, name, args, context):
        ''' Return: total from picking for qty based costs '''
        if not ids : return {}
        result = {}
        for pick in self.browse(cr, uid, ids):
            lcpq = 0.0
            for cost in pick.landed_cost_position_ids:
                if cost.price_type == 'per_unit':
                    lcpq += cost.amount * pick.quantity_total
            result[pick.id] = lcpq
        return result

    def _landing_cost_pick_val(self, cr, uid, ids, name, args, context):
        ''' Return: total from picking for value based costs '''
        if not ids : return {}
        result = {}
        for pick in self.browse(cr, uid, ids):
            lcpv = 0.0
            for cost in pick.landed_cost_position_ids:
                if cost.price_type == 'value':
                    lcpv += cost.amount
            result[pick.id] = lcpv
        return result

    def _landing_cost_picking(self, cr, uid, ids, name, args, context):
        ''' Return: total from picking base costs (base_qty + base_val) '''
        if not ids : return {}
        if context is None: context = {}
        result = kys = its = {}
        val = self._landing_cost_pick_val(cr, uid, ids, name, args, context)
        qty = self._landing_cost_pick_qty(cr, uid, ids, name, args, context)
        kys = list(set(val.keys() + qty.keys()))
        its = val.items() + qty.items()
        result = dict.fromkeys(kys, 0.0)
        for i in its:
            result[i[0]] += i[1]
        return result

    def _landing_cost(self, cr, uid, ids, name, args, context):
        if not ids : return {}
        result = {}
        for pick in self.browse(cr, uid, ids):
            landing_cost = 0.0
            for m in pick.move_lines:
                if m.product_qty > 0.0:
                    landing_cost += m.landing_costs_sm
            result[pick.id] = landing_cost + pick.landing_cost_picking

        return result

    def _landed_cost(self, cr, uid, ids, name, args, context):
        ''' Return: Landed Costs Total Untaxed
            (picked + costs)
        '''
        if not ids : return {}
        #print '--> picking _landed_cost ids: %s \n' % ids
        result = {}
        landed_cost = 0.0
        # landed costs for the line

        for pick in self.browse(cr, uid, ids):
            landed_cost += pick.landing_cost_move + pick.landing_cost_picking + \
                           pick.subt_untaxed
            #print landed_cost, 'landed_cost (accm)...'
            result[pick.id] = landed_cost
        #print result, 'result pickings landed_cost <--\n'
        return result

    def _quantity_total(self, cr, uid, ids, name, args, context):
        ''' Retorna KILOS '''
        if not ids : return {}
        result = {}
        for pick in self.browse(cr, uid, ids):
            quantity_total = 0.0
            for m in pick.move_lines:
                if m.product_qty > 0.0:
                    quantity_total += m.product_qty
            result[pick.id] = quantity_total
        return result

        # _amount_total en purchase
    def _subt_untaxed(self, cr, uid, ids, landed_cost, args, context):
        ''' Retorna $'''
        if not ids : return {}
        #print "--> subt_untaxed picking ids: %s \n" % ids
        result = {}

        for pick in self.browse(cr, uid, ids):
            subt_untaxed = 0.0
            for m in pick.move_lines:
                if m.product_qty > 0.0 and m.price_unit_net > 0.0:
                    subt_untaxed += m.product_qty * m.price_unit_net
            result[pick.id] = subt_untaxed
        #print  result, " result subt_untaxed picking <--\n"
        return result

    def _avg_landed_cost_pick(self, cr, uid, ids, name, args, context):
        '''
        Returns: averaged pick cost (on pick with severals moves)
        '''
        #print "--> _avg_landed_cost_pick ids: %s \n" % ids

        if not ids : return {}
        if context is None: context = {}
        result = kys = its = {}
        cos = self._landed_cost(cr, uid, ids, 'landed_cost', args, context)
        kgs = self._quantity_total(cr, uid, ids, 'quantity_total', args, context)

        if cos and kgs:
            kys = list(set(cos.keys() + kgs.keys()))
            for i in kys:
                if not kgs[i]:
                    result[i] = False
                else:
                    result[i] = cos[i]/kgs[i]
        #print result, '_avg_landed_cost_pick <--'
        return result

    def _avg_cost_pick(self, cr, uid, ids, name, args, context):
        '''
        Returns: averaged picking price (on pickinkgs with severals lines)
        '''
        if not ids : return {}
        if context is None: context = {}
        result = {}

        for pick in self.browse(cr, uid, ids):
            if pick.quantity_total:
                result[pick.id] = pick.subt_untaxed / pick.quantity_total
        return result

    def _landable(self, cr, uid, ids, name, args, context=None):
        '''
            Return form: { po0 : bool, po1 : bool, ... }
            where "bool" is True if purchase pincking has at least one line with
            a landable product otherwise "bool" is False
        '''
        if not ids:
            return {}
        if context is None:
            context = {}
        result = {}

        for pick in self.browse(cr, uid, ids):
            if pick:
                result[pick.id] = False
                for ml in pick.move_lines:
                    if ml and ml.product_id.landable:
                        result[pick.id] = True
                        break
        return result

    _columns = {

         'landing_enable': fields.boolean('Enable Costs', help='It must be checked in order to support landing costs', state={'cancel':[('readonly',True)],'done':[('readonly',True)],'auto':[('readonly',True)],'confirmed':[('readonly',True)]}),
         'landable': fields.function(_landable, type='boolean', string='Picking has product(s) line landable', method=True),
         'landed_cost_position_ids': fields.one2many('landed.cost.position', 'picking_id', 'Landed Costs Positions (picking)', state={'cancel':[('readonly',True)],'done':[('readonly',True)]}),

         'landing_cost_move_val' : fields.function(_landing_cost_move_val, digits_compute=dp.get_precision('Account'), string='Landing Costs Move Value'),
         'landing_cost_move_qty' : fields.function(_landing_cost_move_qty, digits_compute=dp.get_precision('Account'), string='Landing Costs Move Quantity'),
         'landing_cost_move'     : fields.function(_landing_cost_move, digits_compute=dp.get_precision('Account'), string='Landing Costs Move Val+Qty'),

         'landing_cost_pick_val' : fields.function(_landing_cost_pick_val, digits_compute=dp.get_precision('Account'), string='Landing Costs Picking Value'),
         'landing_cost_pick_qty' : fields.function(_landing_cost_pick_qty, digits_compute=dp.get_precision('Account'), string='Landing Costs Picking Quantity'),
         'landing_cost_picking'  : fields.function(_landing_cost_picking, digits_compute=dp.get_precision('Account'), string='Landing Costs Picking Val+Qty'),

         'landing_cost' : fields.function(_landing_cost, digits_compute=dp.get_precision('Account'), string='Total Landing Cost'),
         'landed_cost' : fields.function(_landed_cost, digits_compute=dp.get_precision('Account'), string='Landed Costs Total Untaxed'),

         'quantity_total' : fields.function(_quantity_total, digits_compute=dp.get_precision('Product UoM'), string='Total Quantity'),
         'subt_untaxed' : fields.function(_subt_untaxed, digits_compute=dp.get_precision('Account'), string='Total Product Price'),

         'avg_landed_cost_pick': fields.function(_avg_landed_cost_pick, digits_compute=dp.get_precision('Account'), string='Picking Landed Average Price', help="Avg Picking Landed Cost"),
         'avg_cost_pick': fields.function(_avg_cost_pick, digits_compute=dp.get_precision('Account'), string='Picking Average Price', help="Avg Picking Cost"),
    }

    def _cost_exist(self, cr, uid, ids, context=None):
        '''
            Return form { po0 : bool, po1 : bool, ... }
            where "bool" is True if a landed_cost_position "pick cost" is
            founded, otherwise "bool" will be False
        '''
        if not ids:
            return {}
        if context is None:
            context = {}
        result = {}
        # ipdb.set_trace()
        for pick in self.browse(cr, uid, ids):
            result[pick.id] = False
            if pick.landed_cost_position_ids:
                result[pick.id] = True
                break

        return result

    def onchange_landing_enable(self, cr, uid, ids, landing_enable, context=None):
        '''
            Sirve para no abrir líneas de costo en una orden sin productos costeables.
            Sirve para no cerrar lineas de costo en una orden con productos costeables.
        '''
        if not ids:
            return {}
        if context is None:
            context = {}

        #ipdb.set_trace()
        vals = {}
        warn = {}
        doma = {}

        pick_obj = self.browse(cr,uid,ids)[0]
        if landing_enable:
            landable = pick_obj.landable
            if landable:
                vals = {'landing_enable': landing_enable}
            else:
                landing_enable = False
                vals = {'landing_enable': landing_enable}
                warn = {'title': _('Error'), 'message': _('There\'s no landable products in the incomming shippment you\'re trying enabling costs to.\n\nPlease, check your order o save it if apply.') }
        else:
            cost_exist = self._cost_exist(cr, uid, ids, context)[ids[0]]
            #landable = pick_obj.landable
            if cost_exist:
                landing_enable = True
                vals = {'landing_enable': landing_enable}
                warn = { 'title': _('Error'), 'message': _('Attempt to clear landing cost tab in an incomming shippment with loaded landing costs!\n\nPlease, drop your costs lines before disabling landing cost or save this documment if apply.') }
            else:
                vals = {'landing_enable': landing_enable}

        return  { 'value': vals, 'warning': warn }

stock_picking()




#------------------------------
# wizards inheritances

class stock_partial_move_line(osv.osv_memory):
    '''
        Inheritance from wizard stock.partial.move.line
    '''
    _inherit = "stock.partial.move.line"
    _name = "stock.partial.move.line"
    _columns = {
        ### 'move_ref' : fields.char('Ref', size=24, help='Origen', translate=True),
        'landed_price' : fields.float("Landed Cost", help="Unit Landed Cost for this product line"),
        ###'cost': fields.float("Last Cost", help="Unit Purchase Cost"),
        ###'product_historic_cost_id' : fields.many2one('product.historic.cost', string="Product Historic Cost", required=True, ondelete='CASCADE'),
    }

class stock_partial_move(osv.osv_memory):
    '''
        Inheritance from wizard stock.partial.move
    '''
    _name = "stock.partial.move"
    _inherit = 'stock.partial.move'
    _description = "Partial Move Processing Wizard"
    _columns = {
        'date': fields.datetime('Date', required=True),
        'move_ids' : fields.one2many('stock.partial.move.line', 'wizard_id', 'Moves'),

        # picking_id is not used for move processing, so we remove the required attribute
        # from the inherited column, and ignore it
        'picking_id': fields.many2one('stock.picking', 'Picking'),
        ###'landed_price' : fields.float("Landed Cost", help="Unit Landed Cost for this product line"),
        ###'cost': fields.float("Last Cost", help="Unit Purchase Cost"),
        ###'product_historic_cost_id' : fields.many2one('product.historic.cost', string="Product Historic Cost", ondelete='CASCADE'),
     }

    def _product_cost_for_average_update(self, cr, uid, move):

        res = super(stock_partial_move, self)._product_cost_for_average_update(cr, uid, move)

        #ipdb.set_trace()
        res['move_ref'] = move.picking_id.name or ''
        res['cost'] =  float("%.2f" % move.price_unit_net)
        res['landed_price'] =  move.avg_landed_cost_move
        return res

    def default_get(self, cr, uid, fields, context=None):
        if context is None: context = {}
        # no call to super!
        res = {}
        move_ids = context.get('active_ids', [])
        if not move_ids or not context.get('active_model') == 'stock.move':
            return res
        if 'move_ids' in fields:
            move_ids = self.pool.get('stock.move').browse(cr, uid, move_ids, context=context)
            moves = [self._partial_move_for(cr, uid, m) for m in move_ids if m.state not in ('done','cancel')]
            res.update(move_ids=moves)
        if 'date' in fields:
            res.update(date=time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))
        return res

    def do_partial(self, cr, uid, ids, context=None):
        # no call to super!
        assert len(ids) == 1, 'Partial move processing may only be done one form at a time'
        partial = self.browse(cr, uid, ids[0], context=context)
        phc_obj = self.pool.get('product.historic.cost')
        ###ul_cost_obj = self.pool.get('ul.cost')
        partial_data = {
            'delivery_date' : partial.date
        }
        moves_ids = []
        for move in partial.move_ids:
            move_id = move.move_id.id
            partial_data['move%s' % (move_id)] = {
                'product_id': move.product_id.id,
                'product_qty': move.quantity,
                'product_uom': move.product_uom.id,
                'prodlot_id': move.prodlot_id.id,
            }
            if move.update_cost:
                ###mmid  = string.strip(move.move_id.origin) or ''
                ###mmref = string.strip(move.move_ref) or ''
                ###doc_ref =  "PO: %s | RE: %s  stock_partial_move.do_partial l.616" % ( mmid, mmref )
                phc_vals = {

                            'product_id': move.product_id.id,
                            'product_uom': move.product_uom.id,
                            ###'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                            'name': move.prodlot_id.date,
                            'price': move.cost,
                            'last_landed_cost': move.landed_price,
                            'stock_lot_id': move.prodlot_id.id,
                            ###'order_ref': doc_ref
                    }
                res_id = phc_obj.create(cr, uid, phc_vals, context=None)
                if move.landed_cost_position_ids.is_fake:
                    v_ul_cost = {
                                    'product_historic_cost_id': res_id,
                                    'prod_lot_id': move.prodlot_id.id,
                                    'invoice_line_id': move.landed_cost_position_ids.invoice_line_id
                    }
                    ul_cost_obj.create(cr, uid, v_ul_cost, context=None)

                ''' es de r 103
                prodlot_obj = self.pool.get('stock.production.lot').browse(cr, uid, move.prodlot_id.id)
                prodlot_obj.landed_cost_lot_id = phc_obj.create(cr, uid, phc_vals, context=None)
                '''
            moves_ids.append(move_id)
            if (move.move_id.picking_id.type == 'in') and (move.product_id.cost_method == 'average'):
                partial_data['move%s' % (move_id,)].update(product_price=move.cost,
                                                          product_currency=move.currency.id)
        self.pool.get('stock.move').do_partial(cr, uid, moves_ids, partial_data, context=context)
        return {'type': 'ir.actions.act_window_close'}


class stock_partial_picking_line(osv.TransientModel):
    '''
        Inheritance from wizard stock.partial.picking.line
    '''
    _inherit = "stock.partial.picking.line"
    _name = "stock.partial.picking.line"
    _rec_name = 'product_id'

    _columns = {
        'landed_price' : fields.float("Landed Cost", help="Unit Landed Cost for this product line"),
        'product_historic_cost_id' : fields.many2one('product.historic.cost', string="Product Historic Cost", ondelete='CASCADE'),
    }


class stock_partial_picking(osv.osv_memory):
    '''
        Inheritance from wizard stock.partial.picking
    '''
    _inherit = "stock.partial.picking"
    _logger = logging.getLogger(__name__)


    def _product_cost_for_average_update(self, cr, uid, move):

        res = super(stock_partial_picking, self)._product_cost_for_average_update(cr, uid, move)
        self._logger.debug('res stock_partial_picking `%s`', res)
        #ipdb.set_trace()

        ###res['move_ref'] = move.picking_id.name or ''
        res['cost'] =  float("%.2f" % move.price_unit_net)
        res['landed_price'] =  move.avg_landed_cost_move
        return res
    ''' es del devel
    def _partial_move_for(self, cr, uid, move):
        # ipdb.set_trace()
        # hay que ver que es lo que hace
        partial_move = {
            'product_id' : move.product_id.id,
            'quantity' : move.state in ('assigned','draft') and move.product_qty or 0,
            'product_uom' : move.product_uom.id,
            'prodlot_id' : move.prodlot_id.id,
            'move_id' : move.id,
            'location_id' : move.location_id.id,
            'location_dest_id' : move.location_dest_id.id,
        }
        doc_ref =  'PO: %s | RE: %s  stock_partial_picking._partial_move_for l.680' % (string.strip(move.origin), string.strip(move.picking_id.name or move.wizard_id.picking_id.name or ''))

        phc_vals = {
                    'product_id': move.product_id.id,
                    'product_uom': move.product_uom.id,
                    'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                    'price': move.price_unit_net,
                    'last_landed_cost': move.landed_price,
                    'stock_lot_id': move.prodlot_id.id,
                    'order_ref': doc_ref
        }
        # TODO: garbar uno por cada remito y ver que hacemos en la orden (al pasar a remito)
        # que precio guarda si son varias líneas? debería guardar el prorrateado "solapa" del remito, no de la línea
        # pero no, guarda un precio por línea

        phc_obj = self.pool.get('product.historic.cost')
        res_id = phc_obj.create(cr, uid, phc_vals, context=None)
        if move.landed_cost_position_ids.is_fake:
            ul_cost_obj = self.pool.get('ul.cost')
            v_ul_cost = {
                            'product_historic_cost_id': res_id,
                            'prod_lot_id': move.prodlot_id.id,
                            'invoice_line_id': move.landed_cost_position_ids.invoice_line_id
            }
            ul_cost_obj.create(cr, uid, v_ul_cost, context=None)

        if move.picking_id.type == 'in' and move.product_id.cost_method == 'average':
            partial_move.update(update_cost=True, **self._product_cost_for_average_update(cr, uid, move))
        return partial_move
        '''
    def do_partial(self, cr, uid, ids, context=None):
        if not context:
            context = None
        res = super(stock_partial_picking, self).do_partial(cr, uid, ids, context=context)
        partial = self.browse(cr, uid, ids[0], context=context)
        picking_type = partial.picking_id.type
        phc_obj = self.pool.get('product.historic.cost')
        # get the ul.cost object
        ###ul_cost_obj = self.pool.get('ul.cost')

        for move in partial.move_ids:
            if move.update_cost:
                doc_ref =  'PO: %s | RE: %s ' % (string.strip(move.move_id.origin), string.strip(move.wizard_id.picking_id.name))
                phc_vals = {
                            'product_id': move.product_id.id,
                            'product_uom': move.product_uom.id,
                            'date': move.prodlot_id.date,
                            ### 'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                            'price': move.cost,
                            ###'price': move.price_unit_net,
                            'last_landed_cost': move.landed_price,
                            'stock_lot_id': move.prodlot_id.id,
                            'order_ref': doc_ref
                }

                res_id = phc_obj.create(cr, uid, phc_vals, context=None)

        return {'type': 'ir.actions.act_window_close'}



        """ ###
                if move.landed_cost_position_ids.is_fake:
                    v_ul_cost = {
                            'product_historic_cost_id': res_id,
                            'prod_lot_id': move.prodlot_id.id,
                            'invoice_line_id': move.landed_cost_position_ids.invoice_line_id
                    }
                    ul_cost_obj.create(cr, uid, v_ul_cost, context=None)'''
        return {'type': 'ir.actions.act_window_close'}


create(cr, user , vals, context=None)

Create a new record for the model.
The values for the new record are initialized using the vals argument, and if
necessary the result of default_get().

Parameters:

cr --                    database cursor
user (integer) --        current user id
vals (dictionary) --     field values for new record, e.g {'field_name': field_value, ...}
context (dictionary ) -- optional context arguments, e.g. {'lang': 'en_us', 'tz': 'UTC', ...}

Returns:
            id of new record created


Raises:

AccessError --   if user has no create rights on the requested object
                 if user tries to bypass access rules for create on the requested object
ValidateError -- if user tries to enter invalid value for a field that is
                 not in selection
UserError --     if a loop would be created in a hierarchy of objects a
                 result of the operation (such as setting an object as its own parent)

Note: The type of field values to pass in vals for relationship fields is specific.
Please see the description of the write() method for details about the possible
values and how to specify them.
"""
